2. Crie uma p�gina chamada criacao_personagem.html que contenha umformul�rio para cria��o de um personagem, contendo os seguintesdados:
Nome do Personagem [nome]
Cidade Natal (Thais, Carlin, Venore) [cidade]
Classe (Guerreiro, Arqueiro, Mago) [classe]
Sexo [sexo]
Realizar Tutorial? (Boolean) [tutorial]
O formul�rio deve enviar os dados para
https://venson.net.br/ws/unesc/cadastro.php

3. Estilize o formul�rio do exerc�cio anterior com CSS para que fique comoo mockup. O CSS deve seguir as seguintes especifica��es:
O fundo da p�gina deve ser da cor #222
A fam�lia da fonte deve iniciar com Georgia
A cor da fonte deve ser white
O formul�rio deve estar posicionado ao centro da p�gina e ocupar exatamente metade da tela
Cada campo deve conter margem de 15px para baixo e uma cor defundo #333
Os inputs devem conter cor de fundo #444, sem bordas, ocupando 100% do espa�o dispon�vel.
O bot�o de registro deve ser da cor blueviolet, altura de 50px, negrito, sem borda e tamanho da fonte de 16px